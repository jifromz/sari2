import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
// import {Provider} from 'react-redux';
// import store from './store';
import Main from './components/Router';

// import { library } from "@fortawesome/fontawesome-svg-core";
// import { fab } from "@fortawesome/free-brands-svg-icons";
// import { fas } from "@fortawesome/free-solid-svg-icons";
import UserGlobal from './states/userGlobal';

function App() {
    return (
        <BrowserRouter>
            <Route component={Main} />
        </BrowserRouter>
    );
}

export default App;

ReactDOM.render(<App />, document.getElementById('app'));
