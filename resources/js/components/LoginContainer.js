import React, { useState, useEffect } from "react";
import { Link, Redirect, withRouter } from "react-router-dom";
import FlashMessage from "react-flash-message";

const LoginContainer = props => {
    useEffect(() => {}, []);

    const handleSubmit = e => {
        e.preventDefault();
    };

    return (
        <div className={`my-32 items-center justify-center flex flex-col`}>
            <div
                className={`border border-gray-400 p-16 rounded rounded-md lg:hover:scale-105 duration-300 transform transition-all w-2/5`}
                style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
            >
                <h2
                    className={`font-bold mb-8 text-2xl text-blue-500 text-center`}
                >
                    Login to your account
                </h2>

                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            id="email"
                            type="email"
                            name="email"
                            placeholder="E-mail"
                            className="form-control"
                            required
                            // onChange={this.handleEmail}
                        />
                    </div>
                    <div className="form-group">
                        <input
                            id="password"
                            type="password"
                            name="password"
                            placeholder="Password"
                            className="form-control"
                            required
                            // onChange={this.handlePassword}
                        />
                    </div>
                    <button
                        style={{ backgroundColor: "rgb(0, 52, 102)" }}
                        type="submit"
                        name="singlebutton"
                        className="btn btn-default btn-lg  btn-block mb-10 text-white"
                    >
                        <Link
                            to={`/dashboard`}
                            className="hover:text-blue-600 px-3 tracking-wide
                                transition-all duration-300"
                        >
                            Login
                        </Link>
                    </button>
                </form>
                <p className={`mt-8 text-blue-500 text-center cursor-pointe`}>
                    Don't have an account?
                    {/* <span
                        className="hover:text-blue-600 px-3 tracking-wide
                                transition-all duration-300 cursor-pointer"
                    >
                        Register
                    </span> */}
                    <Link
                        to={`/register`}
                        className="hover:text-blue-600 px-3 tracking-wide
                                transition-all duration-300"
                    >
                        Register
                    </Link>
                    {/* <span className="pull-right">
                        <Link to="/" className="text-black">
                            &nbsp;Back to Home
                        </Link>
                    </span> */}
                </p>
            </div>
        </div>
    );
};
export default LoginContainer;
