import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Grades from "./Components/Grades";
import Profile from "./Components/Profile";
import GradeManager from "./Components/GradeManager";
import Settings from "./Components/Settings";

const Content = () => {
  return (
    <div className="h-full flex-1 p-8 bg-gray-200">
      <Switch>
        <Route exact={true} path="/profile" component={() => <Profile />} />
        <Route
          exact={true}
          path="/grades"
          component={routeParams => (
            <Grades isStudent={true}/>
          )}
        />
         <Route
          exact={true}
          path="/grade-manager"
          component={routeParams => (
            <GradeManager isStudent={true} />
          )}
        />
        <Route
          exact={true}
          path="/settings"
          component={routeParams => (
            <Settings  />
          )}
        />
      </Switch>
    </div>
  );
};

export default React.memo(Content);
