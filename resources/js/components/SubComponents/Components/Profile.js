import React, { useEffect, useState } from "react";
import StudentProfile from './student/StudentProfile';
 
const Profile = props => {
    const [redirect, setRedirect] = useState(props.location);

    useEffect(() => {

        console.log(props);

    },[]);

    return (
        <section style={{ paddingTop: 60 }}>
            <section className="relative text-center bg-center bg-cover">
                <div className={`mx-auto`}>
                    <StudentProfile />
                </div>
            </section>
        </section>
    );
};
export default Profile;
