import React, { useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import ReactDOM from "react-dom";

import Select from "react-select";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const StudentProfile = props => {
    const [selectedGender, setSelectedGender] = useState({
        value: "male",
        label: "Male"
    });
    const [birthdate, setBirthdate] = useState(new Date());

    useEffect(() => {}, []);

    const handleSubmit = e => {
        e.preventDefault();
    };

    const gender = [
        { value: "male", label: "Male" },
        { value: "female", label: "Female" }
    ];

    const onSelect = e => {
        console.log(e);
        setSelectedGender(e);
    };

    const onDateChange = date => {
        setBirthdate(date);
    };

    const handleFileChange = e => {};

    return (
        <div className={`items-center justify-center w-full `}>
            <div
                className={`border border-gray-400 flex flex-col p-16 rounded rounded-md`}
                style={{ boxShadow: "0 5px 28px 4px rgba(0,0,0,0.1)" }}
            >
                <div>
                    <h2
                        className={`font-bold mb-16 text-2xl text-4xl text-blue-500 text-center`}
                    >
                        MyProfile
                    </h2>
                </div>
                <div className={`flex`}>
                    <div className={`flex flex-1 w-full`}>
                        <div
                            className={`flex-1`}
                            //className={`lg:w-4/12`}
                        >
                            <input
                                type={`file`}
                                name={`avatar_path`}
                                className={`hidden`}
                                //   ref={profileInput}
                                onChange={e => handleFileChange(e)}
                            />
                            <div
                                className={`relative mx-auto`}
                                style={{ width: "fit-content" }}
                            >
                                <img
                                    className={`border-4 border-blue-500 h-48 lg:h-56 lg:w-56 object-cover rounded-full shadow-lg w-48`}
                                    src={"/assets/images/profile_default.jpg"}
                                />
                                <span
                                    className={`
                                    absolute bg-white border border-gray-300 bottom-0 cursor-pointer duration-300 flex h-10 hover:bg-blue-500 hover:border-blue-500
                                     hover:text-white items-center justify-center mb-3 mr-3
                                     right-0 rounded-full shadow-md transition-all w-10
                      `}
                                    // onClick={() => handleClick(true)}
                                >
                                    <svg className={`feather-icon h-6 w-6`}>
                                        <use
                                            xlinkHref={`/assets/svg/feather-sprite.svg#edit-2`}
                                        />
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className={`w-3/4`}>
                        <form onSubmit={handleSubmit} className={`w-1/2 ml-16`}>
                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Firstname</label>
                                <input
                                    id="fname"
                                    type="text"
                                    placeholder="First Name"
                                    className="form-control"
                                    required
                                    //onChange={this.handleName}
                                />
                            </div>
                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>
                                    Middlename
                                </label>
                                <input
                                    id="mname"
                                    type="text"
                                    placeholder="Middle Name"
                                    className="form-control"
                                    required
                                    //onChange={this.handleName}
                                />
                            </div>
                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Lastname</label>
                                <input
                                    id="fname"
                                    type="text"
                                    placeholder="Last Name"
                                    className="form-control"
                                    required
                                    //onChange={this.handleName}
                                />
                            </div>

                            <div className={`w-full `}>
                                <div
                                    className={`text-left flex form-group items-center justify-center`}
                                >
                                    <label className={`mr-4 mt-2 w-1/4 text-left`}>
                                        Gender
                                    </label>
                                    <Select
                                        isOptionSelected
                                        name={`gender`}
                                        onChange={e => onSelect(e)}
                                        className={`w-full`}
                                        placeholder={`Select Gender`}
                                        value={selectedGender}
                                        options={gender}
                                    />
                                </div>
                            </div>

                            <div className="flex form-group items-center justify-center">
                                <div className={`flex items-center w-full`}>
                                    <label className={`mr-4 mt-2 w-1/4 text-left`}>
                                        Birthdate
                                    </label>
                                    {/* <label className={`items-center mt-3 text-center text-gray-600`}>Birthdate</label> */}
                                    <DatePicker
                                        selected={birthdate}
                                        onChange={date => onDateChange(date)}
                                        className={`border border-gray-400 my-4 px-3 py-2 react-datepicker-wrapper rounded text-left w-full`}
                                    />
                                </div>
                            </div>

                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Address</label>
                                <textarea
                                    id="mname"
                                    type="text"
                                    placeholder="Address"
                                    className="form-control"
                                    required
                                    //onChange={this.handleName}
                                />
                            </div>

                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Email</label>
                                <input
                                    id="email"
                                    type="email"
                                    name="email"
                                    placeholder="E-mail"
                                    className="form-control"
                                    required
                                    //onChange={this.handleEmail}
                                />
                            </div>

                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Phonenumber</label>
                                <input
                                    id="phone"
                                    type="text"
                                    name="phone"
                                    placeholder="Contact Number"
                                    className="form-control"
                                    required
                                    //onChange={this.handleEmail}
                                />
                            </div>

                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>Password</label>
                                <input
                                    id="password"
                                    type="password"
                                    name="password"
                                    placeholder="Password"
                                    className="form-control"
                                    required
                                    //onChange={this.handlePassword}
                                />
                            </div>
                            <div className="flex form-group items-center justify-center">
                                <label className={`mr-4 mt-2 w-1/4 text-left`}>
                                    Confirm Password
                                </label>
                                <input
                                    id="password_confirm"
                                    type="password"
                                    name="password_confirm"
                                    placeholder="Confirm Password"
                                    className="form-control"
                                    required
                                    //onChange={this.handlePasswordConfirm}
                                />
                            </div>
                            <div className={`flex`}>
                                <button
                                    style={{
                                        backgroundColor: "rgb(0, 52, 102)"
                                    }}
                                    type="submit"
                                    name="singlebutton"
                                    className="btn btn-default btn-lg mb-10 mt-3 text-white ml-32"
                                >
                                    Update
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
// 2.8
export default withRouter(StudentProfile);
