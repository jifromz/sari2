import React, { useEffect, useState } from "react";
 
const Grades = props => {
    const [redirect, setRedirect] = useState(props.location);
    return (
        <section style={{ paddingTop: 120 }}>
            <section className="relative text-center bg-center bg-cover">
                <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
                    Grades
                </div>
            </section>
        </section>
    );
};
export default Grades;
