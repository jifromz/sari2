import React, { useEffect, useState } from "react";
import RegisterContainer from "./RegisterContainer";
const Register = (props) => {
    const [redirect, setRedirect] = useState(props.location);

    return (
        <section style={{ paddingTop: 120 }}>
            <section className="relative text-center bg-center bg-cover">
                <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
                    {/* <RegisterContainer redirect={redirect} /> */}
                    <RegisterContainer />
                </div>
            </section>
        </section>
    );
};
export default Register;
