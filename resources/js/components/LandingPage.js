import React, { useEffect, useState } from "react";
import MainLayout from '../layout/MainLayout';

const LandingPage = () => {
    const [toogleLogin, setToogleLogin] = useState(false);

    const changeDisplay = () => {
        setToogleLogin(!toogleLogin);
    };

    useEffect(() => {}, []);

    return (
        <MainLayout>
            
        </MainLayout>
    );
};

export default LandingPage;
