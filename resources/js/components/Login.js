import React, { useState, useEffect } from "react";
import LoginContainer from "./LoginContainer";
import MainLayout from "../layout/MainLayout";
import { withRouter } from "react-router-dom";

const Login = ({changeDisplay}) => {
    

    return (
            <section style={{ paddingTop: 120 }}>
                <section
                    className="relative text-center bg-center bg-cover"
                >
                     <div className={`mx-auto`} style={{ maxWidth: 1366 }}>
                        {/* <LoginContainer redirect={redirect} /> */}
                        <LoginContainer changeDisplay={changeDisplay} /> 
                    </div>
                </section>
            </section>
    );
};
export default withRouter(Login);
