import React from 'react';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import MainLayout from '../layout/MainLayout';
import Login from './Login';
import Register from './Register';
import NotFound from './NotFound';
// User is LoggedIn
import PrivateRoute from './PrivateRoute';
import Dashboard from './Dashboard';

const Main = props => (
    <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/dashboard" component={Dashboard} />
        {/* <Route path="/login" component={Login} /> */}
        <Route path="/register" component={Register} />
        {/* <PrivateRoute path="/dashboard" component={Dashboard} /> */}
        <Route component={NotFound} />
    </Switch>
);
export default Main;
