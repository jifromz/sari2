import React from 'react';
const Footer = () => {
    return (
        <footer className={`flex items-center justify-center mx-auto pt-10 px-6 relative z-50`} style={{backgroundColor: '#003466', maxWidth: 1366}}>
            <div className={`flex items-center justify-center text-white`}>
                <span>Footer</span>
            </div>
        </footer>
    );
};
export default Footer;
