import React, {useState, useEffect} from 'react';
import {Link, withRouter} from 'react-router-dom';

const Header = props => {
    const [user, setUser] = useState(props.userData);
    const [isLoggedIn, setIsLoggedIn] = useState(props.userIsLoggedIn);

    const logOut = () => {
        let state = localStorage['appState'];

        let AppState = JSON.parse(state);
        setIsLoggedIn(AppState.isLoggedIn);
        setUser(AppState.user);

        props.history.push('/login');
    };

    const aStyle = {
        cursor: 'pointer',
    };

    const links = [
        {
            name: 'Login',
            route: '/login',
        },
        {
            name: 'Register',
            route: '/register',
        },
    ];

    const navLinks = () => {
        return links.map((link, key) => {
            return (
                <Link
                    key={key}
                    to={link.route}
                    className={`text-white
                mb-6 lg:mb-0
                hover:text-palette-teal px-3 tracking-wide
                transition-all duration-300
              `}>
                    <span>{link.name}</span>
                </Link>
            );
        });
    };

    return (
        <header
            style={{maxWidth: 1366, backgroundColor: '#003466'}}
            className={`relative z-50 flex items-center mx-auto justify-center lg:justify-between px-6 lg:px-16 pt-10`}>
            <div>
                {/* <Link to="/">
                    <img
                        src="/assets/images/roofing-company-logo.jpg"
                        style={{height: 80, maxWidth: 120}}
                    />
                </Link> */}
            </div>
            {!isLoggedIn && <div>{navLinks()}</div>}
        </header>
    );
};
export default withRouter(Header);
