import React, { useState, useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Login from "../components/Login";
import Register from "../components/Register";
 

const Mainlayout = ({children}) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [user, setUser] = useState({});
    const [toogleRegister, setToogleRegsiter] = useState(false);

    const showRegisterCompnent = () => {
        setToogleRegsiter(!toogleRegister);
    };

    useEffect(() => {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            setIsLoggedIn(AppState.isLoggedIn);
            setUser(AppState.user);
        }
    }, []);

    return (
        <>
            <Header userData={user} userIsLoggedIn={isLoggedIn} />

                <main>{children}</main>
            
            <Footer />
        </>
    );
};

export default Mainlayout;
