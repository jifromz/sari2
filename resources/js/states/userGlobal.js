import React from "react";
import useGlobalHook from "use-global-hook";

const actions = {
  setState: (store, payload) => {
    Object.keys(payload).forEach(key => {
      store.setState({ [key]: payload[key] });
    });
  }
};


const initialState = {
 
};

const UserGlobal = useGlobalHook(React, initialState, actions);

export default UserGlobal;
